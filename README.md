# 基于Luckysheet实现的协同编辑在线表格

------

本项目由 https://gitee.com/DilemmaVi/ecsheet 分支而来，修复了几个BUG。
该项目基于国内最火的开源前端在线表格**Luckysheet** 可实现类似于 腾讯文档、金山云文档等在线Office工具的 在线表格功能。
此项目可以实现协同编辑，很利于在内网部署使用。

本项目后端的语言是Java，主要技术栈如下：

> * 框架：SpringBoot + Websocket
> * 数据库：MongoDB 4.4.0
> * 前端核心：Luckysheet


### [友情链接：Luckysheet](https://github.com/mengshukeji/Luckysheet)
其文档：https://mengshukeji.gitee.io/LuckysheetDocs/zh/

> 🚀Luckysheet is an online spreadsheet like excel that is powerful, simple to configure, and completely open source.

------
## 演示地址: [点击访问](http://mars.free.idcfengye.com/index)


## QuickStart

由于后端是基于Java和mongo的，需要提前安装相关环境，如想快速体验，建议使用Docker，本项目也经docker环境验证可以正常运行。

### 1. 安装Docker环境

参考阿里云方案：https://developer.aliyun.com/article/110806

### 2. 使用Docker 安装mongodb并启动

参考菜鸟教程方案：https://www.runoob.com/docker/docker-install-mongodb.html
代码里面的mongo账户密码为 admin/123456，请保持一致

### 3. 打包程序docker镜像

* 在服务器新建一个docker文件夹，将maven打包好的jar包和Dockerfile文件复制到服务器的docker文件夹下,执行以下语句

```shell
docker build -t ecsheet .
```
* 启动容器
```shell
docker run -d --name ecsheet --link mongo:mongo -p 9999:9999 ecsheet
```

### 4. 访问地址
http://{你服务器的ip地址}:9999/index